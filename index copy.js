import { AppRegistry, YellowBox } from "react-native";

import App from "./src/app/App";

YellowBox.ignoreWarnings(["Warning:", "Module RCTImageLoader", "RCTRootView cancelTouches"]);

AppRegistry.registerComponent("testproject", () => App);
