/* eslint-disable no-throw-literal */
import axios from "axios";

class Request {
	constructor(host, apiSecret) {
		this.host = host;
		this.secret = apiSecret;
	}

	setToken = (token) => {
		this.token = token;
	}


	get = (route) => this._request(route, "GET");

	post = (route, body) => this._request(route, "POST", body);

	patch = (route, body) => this._request(route, "PATCH", body);

	delete = (route) => this._request(route, "DELETE");

	_request = async (route, method, body) => {
		try {
			console.debug(method, this.host, route, this.formdata);
			const payload = {
				method,
				headers: {
					Accept: "application/json",
					"Content-Type": this.contentype || "application/json",
					// "x-secret": this.secret,
				},
			};

			if (method !== "GET" && method !== "HEAD") {
				payload.data = JSON.stringify(body);
			}

			const url = `${this.host}${route}`;

			return await this._sendHttpRequest3(url, payload);

		} catch (e) {
			throw e;
		}
	}

	_sendHttpRequest = async (url, payload) => {
		payload.url = url;
		console.log("REQUEST PAYLOAD");
		console.log(payload);

		const response = await fetch(url, payload);
		console.log("response", response);

		if (response.ok === false){
			throw await response.json();
		}

		return response.json().catch(() => {
			return {message: "Success"};
		});
	}

	_sendHttpRequest3 = async (url, payload) => {
		payload.url = url;
		// payload.crossDomain = true;
		console.log("REQUEST PAYLOAD");
		console.log(payload);

		const response = await axios(payload);
		console.log("response", response);


		if (response.status === 400 || response.status === 401 ||
			response.status === 402 || response.status === 403 || response.status === 404){
			throw await "Something went wrong";
		}
		
		return response;

		// throw await {code: response.status,
		// 	data: response.data};
	}
}

export default Request;
