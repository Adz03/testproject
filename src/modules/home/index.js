import Homescreen from "./containers/Homescreen";
import reducers from "./reducers";

export const home = reducers;

export default Homescreen;
