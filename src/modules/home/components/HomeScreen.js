/* eslint-disable */
import React from "react";
import {View, Text, StyleSheet, StatusBar, Image} from "react-native";
import {Icon} from "react-native-elements";
import ExpanableList from "react-native-expandable-section-flatlist";
import Resource from "__src/resources";
const {Color, Res} = Resource;

class HomeScreen extends React.PureComponent {
	
	static navigationOptions = {
		setHeaderTitle: "Home",
	}

	componentDidMount(){
		const {actions} = this.props;

		actions.getAllCategories();
	}

	_setImage(type){
		switch (type){
		case "Fruits":
			return Res.get("fruit");
		case "Vegetable":
			return Res.get("vege");
		case "Meat":
			return Res.get("meat");
		default:
			return null;
		}
	}
	_renderSection = (section)  => {

		return (
			<View style={styles.sectionStyle} >
				<Image style={styles.sectionImage} source={this._setImage(section)}  />
				<Text style={styles.sectionText}>{section}</Text>
				<Icon type='font-awesome' name='caret-down'  size={20} color="black" />
			</View>
		);
	};
	
	_renderRow = (rowItem) => (
		<View activeOpacity={0.8} style={styles.rowStyle} >
			<View style={styles.rowView}/>
			<Image style={styles.sectionImage} source={this._setImage(rowItem.category)}  />
			<Text style={styles.rowText}>{rowItem.child_name}</Text>
		</View>
	);

	render() {
		const {home: {getAllCategories}} = this.props;

		return (
			<View style={styles.container}>
				<StatusBar backgroundColor={Color.StatusBar} barStyle="light-content" />
				<View style={styles.body}>
					<ExpanableList
						ref={(e) => this.expand = e}
						dataSource={getAllCategories}
						headerKey="category_name"
						memberKey="child"
						renderRow={this._renderRow}
						renderSectionHeaderX={this._renderSection}
						// headerOnPress={(i, bol) => this.expand.setSectionState(0, false)}
						isOpen={false}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {flexShrink: 1, width: "100%", height: "100%", backgroundColor: Color.StatusBar},
	body: {flex: 1, backgroundColor: "white"},

	sectionStyle: {flexDirection: "row",
		backgroundColor: Color.colorPrimary,
		marginBottom: 1, padding: 10, alignItems: "center"},
	sectionImage: {width: 30, height: 30},
	sectionText: { flex: 1, marginLeft: 10, fontFamily: "roboto", fontWeight: "bold", fontSize: 18, color: Color.white},

	rowStyle: {flexDirection: "row", backgroundColor: Color.white,  borderBottomColor: Color.gray04, borderBottomWidth: StyleSheet.hairlineWidth},
	rowView: {width: 5, backgroundColor: Color.colorAccent},
	rowText: {padding: 8, fontFamily: "roboto", fontSize: 16, color: Color.gray05},
});

export default HomeScreen;
