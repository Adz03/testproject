export const LOGOUT = "logout/types/LOGOUT";


export const GET_CATEGORIES_LOAD = "home/types/GET_CATEGORIES_LOAD";
export const GET_CATEGORIES = "home/types/GET_CATEGORIES";
export const GET_CATEGORIES_FAILED = "home/types/GET_CATEGORIES_FAILED";
