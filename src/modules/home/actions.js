/* eslint-disable no-throw-literal */
import * as Types from "./types";
import * as globals from "__src/globals";

export const getAllCategories = () => (
	async (dispatch) => {
		try {
			dispatch({ type: Types.GET_CATEGORIES_LOAD });

			const result = await globals.API.get("/category");

			if (result && result.data.length > 0){
				dispatch({ type: Types.GET_CATEGORIES, data: result.data });
			} else {
				throw "Username or Password is incorrect";
			}

		} catch (err) {
			dispatch({ type: Types.GET_CATEGORIES_FAILED, error: err });
		}
	}
);
