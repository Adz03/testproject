import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as ActionCreators from "../actions";
import * as ActionCreatorsLogin from "__src/modules/login/actions";
import HomeScreen from "../components/HomeScreen";

const mapStateToProps = ({ home, login}) => ({
	home, login,
});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators({...ActionCreators, ...ActionCreatorsLogin}, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
