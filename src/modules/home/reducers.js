import { combineReducers } from "redux";
import * as Types from "./types";

const getAllCategories = (state = [], action) => {
	switch (action.type){
	case Types.GET_CATEGORIES:
		return action.data;
	default:
		return state;
	}
};

export default combineReducers({
	getAllCategories,
});
