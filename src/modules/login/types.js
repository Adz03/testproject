export const SET_LOGIN_DETAILS = "login/types/SET_LOGIN_DETAILS";
export const LOGOUT = "logout/types/LOGOUT";
export const LOGIN_RESET = "login/types/LOGIN_RESET";
export const LOGGING_INPROGRESS = "login/types/LOGGING_INPROGRESS";
export const LOGIN_SUCCESS = "login/types/LOGIN_SUCCESS";
export const LOGIN_FAILED = "login/types/LOGIN_FAILED";