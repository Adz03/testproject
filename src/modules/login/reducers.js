import { combineReducers } from "redux";
import _ from "lodash";

import * as Types from "./types";

const initialInput = {
	email: "",
	password: "",
};

const inputLoginDetails = (state = initialInput, action) => {
	switch (action.type){
	case Types.SET_LOGIN_DETAILS:
		return action.data;
	case Types.LOGIN_SUCCESS:
	case Types.LOGOUT:
		return initialInput;
	default:
		return state;
	}
};

const isLoggingIn = (state = false, action) => {
	switch (action.type) {
	case Types.LOGGING_INPROGRESS:
		return true;
	case Types.LOGIN_SUCCESS:
	case Types.LOGIN_FAILED:
	case Types.LOGIN_RESET:
	case Types.SET_LOGIN_DETAILS:
		return false;
	default:
		return state;
	}
};

const isLoggedIn = (state = false, action) => {
	switch (action.type) {
	case Types.LOGIN_SUCCESS:
		return true;
	case Types.LOGOUT:
		return false;
	default:
		return state;
	}
};

const isFailed = (state = false, action) => {
	switch (action.type) {
	case Types.LOGIN_FAILED:
		return true;
	case Types.LOGIN_RESET:
	case Types.SET_LOGIN_DETAILS:
	case Types.LOGGING_INPROGRESS:
		return false;
	default:
		return state;
	}
};

const session = (state = {}, action) => {
	switch (action.type) {
	case Types.LOGIN_SUCCESS:
		return _.merge({...state}, action.data);
	case Types.LOGOUT:
		return {};
	default:
		return state;
	}
};

export default combineReducers({
	inputLoginDetails,
	isLoggingIn,
	isLoggedIn,
	isFailed,
	session,
});
