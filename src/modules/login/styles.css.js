import {StyleSheet} from "react-native";
// import {getStatusBarHeight} from "__src/resources/customize/StatusBarHeight";
import Color from "__src/resources/styles/color";

export default StyleSheet.create({
	container: {flexShrink: 1, width: "100%", height: "100%", backgroundColor: Color.black},
	body: {flex: 1},
	imagebackground: {width: "100%", height: "100%", position: "absolute"},
	
	// LoginForm
	flex1: {flex: 1, backgroundColor: "white"},
	flexRow: {flexDirection: "row"},
	txtLanguage: {color: Color.Header, fontFamily: "Roboto", fontSize: 13, marginRight: 3},
	marginTop10: {marginTop: 10},
	height90: {height: "93%"},
	top: {height: "45%", alignItems: "center",  justifyContent: "space-between", padding: 10},
	middle: {flex: 1, height: "55%", paddingHorizontal: 20},
	bottom: { height: "7%", backgroundColor: Color.StatusBar, justifyContent: "center", alignItems: "center" },
	imageLogo: {width: 145, height: 145, marginBottom: 10},
	txtError: { color: "red", fontFamily: "Roboto", fontSize: 15, textAlign: "center", marginBottom: 5},
	txtHelpContainer: {fontSize: 13, fontFamily: "Roboto", marginVertical: 10, color: Color.white, textAlign: "center" },
	txtHelp: {fontWeight: "bold", color: Color.colorPrimary},
	txtGetHelpContainer: {fontSize: 14, marginVertical: 20, color: Color.colorPrimary, fontFamily: "Roboto", textAlign: "center" },
	txtGetHelpContainer2: {fontSize: 14, marginVertical: 20, color: Color.Standard2, fontFamily: "Roboto", textAlign: "center" },
	txtfb: {color: Color.blue, fontSize: 15, fontFamily: "Roboto"},
	btnfb: {height: 45, marginTop: 15, alignItems: "center",
		backgroundColor: "transparent", flexDirection: "row", justifyContent: "center" },
	loadview: {width: 10, height: 10},
	btnlogin: {height: 45, borderBottomWidth: 6},
	viewLogin: {flexShrink: 1, borderRadius: 5, backgroundColor: Color.colorPrimary, marginTop: 15},
	viewFBWrapper: {flexDirection: "row", alignItems: "center", justifyContent: "center", marginVertical: 10},
	viewLine: {flex: 1, height: 0.7, backgroundColor: Color.white},
	txtSocial: {fontSize: 13, marginHorizontal: 10, fontFamily: "Roboto", color: Color.white },
	imgFb: {width: 40, height: 40, marginRight: 8},

})
;
