import Resource from "__proj/src/resources";
import React, { PureComponent } from "react";
import { View, StatusBar, Image, SafeAreaView } from "react-native";
import PropTypes from "prop-types";
import styles from "../styles.css";
import LoginForm from "./LoginForm";
import _ from "lodash";
import {Navigation} from "react-native-navigation";

const {Color} = Resource;

export default class LoginScreen extends PureComponent {
	static navigationOptions = {
		header: null,
	}
	constructor(props){
		super(props);
		this.state = {
			isLoginScreen: true,
		};
	}

	componentDidUpdate(prevProps){
		const { login } = this.props;
		const { session } = login;

		if (!_.isEqual(prevProps.login.session, session) && !_.isEmpty(session)){
			Navigation.push(this.props.componentId, {
				component: {
					name: "Home",
					passProps: {
						screenType: "Home",
					},
					options: {
						topBar: {
							background: {
								color: Color.Header,
							},
							title: {
								color: "white",
								text: "HOME",
							},
							backButton: {
								color: "white",
							},
						},
					},
				},
			});
		}
	}
	
	renderLoginForm = () => (
		<LoginForm {...this.props} />
	);


	render() {
		return (
			<SafeAreaView style={styles.container}>
				<View style={styles.container}>
					<StatusBar barStyle="light-content" backgroundColor={Color.Header} />
					{this.renderLoginForm()}
				</View>
			</SafeAreaView>
		);
	}
}

LoginScreen.propTypes = {
	actions: PropTypes.object,
	login: PropTypes.object,
	navigation: PropTypes.object,
	wallet: PropTypes.object,
};
