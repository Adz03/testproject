/* eslint-disable react-native/no-inline-styles */
import Resource from "__src/resources";
import TxtInput from "__src/components/TxtInput";
import React, { PureComponent } from "react";
import { Text, View, Image } from "react-native";
import PropTypes from "prop-types";
import Button from "__src/components/Button";
import styles from "../styles.css";
import { Icon } from "react-native-elements";
import _ from "lodash";

const { Res } = Resource;

class LoginForm extends PureComponent {
	static navigationOptions = {
		header: null,
	};

	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			viewp: true,
			forgot: false,
			showFbLoginModal: false,
			isFBreg: false,
			message: "",
			error: {},
			user: {},
		};
	}

	componentDidMount() {
		const { actions } = this.props;

		actions.setLoginDetails({});
	}

	_handleChangeInput = (type) => (value) => {
		const error = {};
		const { actions, login } = this.props;
		const newInput = _.merge({}, login.inputLoginDetails);

		switch (type) {
		case 1:
			if (_.isEmpty(value)) {
				error.email = "Email is required";
			}
			newInput.email = value.trim();
			break;
		case 2:
			if (_.isEmpty(value)) {
				error.password = "Password is required";
			}

			newInput.password = value.trim();
			break;
		}

		this.setState({ error });
		actions.setLoginDetails(newInput);
	};

	_onSubmit = () => {
		const { actions, login } = this.props;
		const error = {};

		if (_.isEmpty(login.inputLoginDetails.email)) {
			error.email = "Email is required.";
			this.email.focus();
		} else if (_.isEmpty(login.inputLoginDetails.password)) {
			error.password = "Password is required.";
			this.password.focus();
		}

		if (_.isEmpty(error)) {
			actions.login(login.inputLoginDetails);
		} else {
			this.setState({ error });
		}
	};

	errMessage = () => {
		const { isUserOutletActive, isFailed, failLogInWithFb } = this.props.login;

		if (isUserOutletActive) {
			return "This account is not active";
		} else if (isFailed) {
			return "Username or Password is incorrect";
		} else if (failLogInWithFb) {
			return "Username or Password is incorrect";
		}

		return null;
	};


	render() {
		const { login: { isLoggingIn, inputLoginDetails },
			navigation } = this.props;
		const { error } = this.state;

		return (
			<View style={styles.flex1}>
				<View style={styles.height90}>
					<View style={styles.top}>
						<View style={styles.flexRow}>
							<Text style={styles.txtLanguage}>English (United States)</Text>
							<Icon
								name="chevron-down"
								type="evilicon"
								size={20}
								color="black"
							/>
						</View>
						<Image
							style={styles.imageLogo}
							source={Res.get("mac_desktop")}
							resizeMode={"contain"}
						/>
					</View>
					<View style={styles.middle}>
						<TxtInput
							logintype
							onRef={(e) => (this.email = e)}
							onFocus={() => this.setState({ usernameFocus: true })}
							onBlur={() => this.setState({ usernameFocus: false })}
							round
							placeholder="Email"
							isFocus={this.state.usernameFocus}
							onChangeText={this._handleChangeInput(1)}
							value={inputLoginDetails.email}
							returnKeyType="next"
							icon="profile_icon"
							err={this.errMessage() || error.email}
						/>

						<TxtInput
							logintype
							style={styles.marginTop10}
							onRef={(e) => (this.password = e)}
							onFocus={() => this.setState({ passwordFocus: true })}
							onBlur={() => this.setState({ passwordFocus: false })}
							round
							isFocus={this.state.passwordFocus}
							onChangeText={this._handleChangeInput(2)}
							value={inputLoginDetails.password}
							returnKeyType="next"
							placeholder="Password"
							secureTextEntry={this.state.viewp}
							icon="lock_icon"
							icon2="view_icon"
							onSubmitEditing={this._onSubmit}
							viewPass={() => this.setState({ viewp: !this.state.viewp })}
							err={error.password}
						/>

						<View style={[styles.viewLogin, { elevation: 2 }]}>
							<Button
								onPress={this._onSubmit}
								loading={isLoggingIn}
								style={styles.btnlogin}
								label="Log in"
							/>
						</View>

						<Text style={[styles.txtGetHelpContainer, {}]}>
							<Text
								suppressHighlighting
								onPress={() =>
									navigation.navigate("ForgotPassword", {
										title: "Forgot Password" })
								}>
									Forgot Password?{" "}
							</Text>
								/
							<Text
								suppressHighlighting
								onPress={() =>
									navigation.navigate("ForgotUsername", {
										title: "Forgot Username",
									})
								}>
								{" "}
									Forgot Username?{" "}
							</Text>
						</Text>
					</View>
				</View>
				<View style={styles.bottom}>
					<Text style={styles.txtHelpContainer}>
							Dont have an account?
						<Text
							suppressHighlighting
							onPress={() => this.props.navigation.navigate("Fullname")}
							style={styles.txtHelp}>Sign up. </Text>
					</Text>
				</View>
			</View>
		);
	}
}

LoginForm.propTypes = {
	actions: PropTypes.object,
	session: PropTypes.object,
	navigation: PropTypes.object,
	login: PropTypes.object,
};

export default LoginForm;
