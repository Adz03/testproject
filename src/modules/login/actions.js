/* eslint-disable */
import _ from "lodash";
import * as Types from "./types";
import * as globals from "__src/globals";

global.navigator = {
  userAgent: 'node',
}

export const setLoginDetails = (data) => ({
	type: Types.SET_LOGIN_DETAILS,
	data,
});


export const login = (params) => (
	async (dispatch) => {
		try{
			dispatch({ type: Types.LOGGING_INPROGRESS });

			const result = await globals.API.get(`/users?email=${params.email}&password=${params.password}`);

			if(result && result.data.length > 0){
				dispatch({ type: Types.LOGIN_SUCCESS, data: result.data[0] });
			}else{
				throw "Username or Password is incorrect"
			}

		} catch (err) {
			dispatch({ type: Types.LOGIN_FAILED, error: err });
		}
	}
);