/* eslint-disable */
let instance = null;

import Color from "./styles/color";
import _ from "lodash";

class ImgResource {
	static getInstance() {
		if (!instance) {
			instance = new ImgResource();
		}
		
		return instance;
	}

	constructor() {
		
		this.imgs = {
			view_icon: require("./images/view_icon.png"),
			lock_icon: require("./images/lock_icon.png"),
			profile_icon: require("./images/profile_icon.png"),
			mac_desktop: require("./images/devices/mac_dektop.png"),
			windows_desktop: require("./images/devices/windows_dektop.png"),

			fruit: require("./images/fruit.png"),
			vege: require("./images/vege.png"),
			meat: require("./images/meat.png"),

		};

		this.string = {
		};

		this.svg = {

		};
	}

	get(name) {
		return this.imgs[name];
	}

	getString(name) {
		return this.string[name];
	}

	getSVG(name) {
		return this.svg[name];
	}
}

const Resource = {
	Color, Res: ImgResource.getInstance(),
};

export default Resource;
