/* eslint-disable */
import { combineReducers } from "redux";
import { login } from "../modules/login";
import { home } from "../modules/home";

const reducer = combineReducers({
	login,
	home,
});

const rootReducer = (state = {}, action) => {
	switch (action.type) {
	case "REDUX_STORAGE_LOAD": {
		const newState = {...state};

		return newState;
	}
	case "logout/types/LOGOUT": {
		const newState = { ...state };

	
		return reducer(newState, action);
	}
	default:
		return reducer(state, action);
	}
};

export default rootReducer;
