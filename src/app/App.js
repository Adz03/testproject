/* eslint-disable */
import React from "react";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "remote-redux-devtools";
import {createLogger} from "redux-logger";
import * as reduxStorage from "redux-storage";
import debounce from "redux-storage-decorator-debounce";
import createEngine from "./AsyncStorage";
import AppReducer from "./reducers";
const logger = createLogger({
	// ...options
});
import {Navigation} from "react-native-navigation";

import Home from "../modules/home";
import Login from "../modules/login";

const engine = debounce(createEngine("sample-key"), 1500);
const storage = reduxStorage.createMiddleware(engine);

const composeEnhancers = composeWithDevTools({ realtime: true });
const store = createStore(reduxStorage.reducer(AppReducer),
	composeEnhancers(applyMiddleware(thunk, logger, storage))
);

export default () => {
	Navigation.registerComponent('Login', () => (props) => (
		<Provider store={store}>
			<Login {...props} />
		</Provider>
	), () => Login);
	Navigation.registerComponent('Home', () => (props) => (
		<Provider store={store}>
			<Home {...props} />
		</Provider>
	), () => Home);
};

