/* eslint-disable react-native/no-inline-styles */
import React, {PureComponent} from "react";
import {View, TouchableWithoutFeedback, Animated, Text, StyleSheet} from "react-native";
import PropTypes from "prop-types";
import Loading from "__src/components/Loading";
import Resources from "__src/resources";
import {Icon} from "react-native-elements";

const {Color} = Resources;

export default class Button extends PureComponent{
	constructor(props){
		super(props);

		this.handlePressIn = this.handlePressIn.bind(this);
		this.handlePressOut = this.handlePressOut.bind(this);
		this.animatePress = new Animated.Value(1);
	}

	handlePressIn(){
		Animated.spring(this.animatePress, {
			toValue: 0.96,
		}).start();
	}
		
	handlePressOut(){
		Animated.spring(this.animatePress, {
			toValue: 1,
		}).start();
	}

	_renderComponent(){
		switch (this.props.icon) {
		
		case "Cart":
			return (
				<Icon
					type='evilicon'
					name='cart'
					color={Color.colorPrimary}
					size={25}
				/>
			);
		default:
			return null;
		}
	}

	render(){
		const {style, onPress, loading,
			color, label, labelStyle, children} = this.props;
		const animatedStyle = {
			transform: [{ scale: this.animatePress}],
		};

		if (loading){
			return (
				<View style={[styles.container, {flexDirection: "column"}, style]}>
					<Loading size="small" color={color || "white"}/>
				</View>
			);
		}
    
		return (
			<TouchableWithoutFeedback
				onPress={onPress}
  			onPressIn={this.handlePressIn}
  			onPressOut={this.handlePressOut}>
				<Animated.View style={[styles.container, style, animatedStyle]}>
					{this._renderComponent()}
					{children || <Text style={[styles.text, labelStyle]}>{label}</Text>}
				</Animated.View>
			</TouchableWithoutFeedback>
		);
	}
}

Button.propTypes = {
	style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
	labelStyle: PropTypes.object,
	onPress: PropTypes.func,
	icon: PropTypes.string,
	loading: PropTypes.bool,
	color: PropTypes.string,
	label: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
	children: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
};

const styles = StyleSheet.create({
	container: {height: 40, alignItems: "center", flexDirection: "row",
		backgroundColor: Color.colorPrimary, borderRadius: 5, borderBottomWidth: 4,
		borderBottomColor: Color.colorPrimaryDark, justifyContent: "center" },
	text: {color: Color.white, fontSize: 14, fontFamily: "Roboto"},
});
