import {Navigation} from "react-native-navigation";
import Home from "./src/modules/home";
import Login from "./src/modules/login";
import RegisterScreen from "./src/app/App";
import NavOpt from "./src/components/navOpt";
import Resources from "__src/resources";
const {Color} = Resources;

// Navigation.registerComponent("App", () => App);
// Navigation.registerComponent("Home", () => Home);
RegisterScreen();

Navigation.events().registerAppLaunchedListener(() => {
	Navigation.setRoot({
		root: {
			stack: {
				id: "InitStack",
				children: [
					{
						component: {
							name: "Login",
							options: {
								topBar: {
									visible: false,
								},
							},
						},
					},
				],
			},
		},
	});
});
